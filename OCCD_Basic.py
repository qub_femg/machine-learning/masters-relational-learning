import sys
import numpy as np
from OCC.Core._BRepGProp import brepgprop_SurfaceProperties, brepgprop_VolumeProperties
import Python_Basic as PB
import PySimpleGUI as sg
import os

from OCC.Core.gp import gp_Pnt, gp_Vec
from OCC.Core.ShapeAnalysis import ShapeAnalysis_Surface
from OCC.Core.BRep import BRep_Tool, BRep_Tool_Triangulation
from OCC.Core.GeomAbs import GeomAbs_Plane, GeomAbs_Cylinder, GeomAbs_Torus, GeomAbs_Cone, GeomAbs_Sphere, \
    GeomAbs_BezierSurface, GeomAbs_BSplineSurface, GeomAbs_SurfaceOfRevolution, GeomAbs_SurfaceOfExtrusion, \
    GeomAbs_OffsetSurface, GeomAbs_OtherSurface
from OCC.Core.GeomAbs import GeomAbs_BezierCurve, GeomAbs_BSplineCurve, GeomAbs_Line, GeomAbs_Parabola, GeomAbs_Ellipse, \
    GeomAbs_Circle, GeomAbs_Hyperbola, GeomAbs_OtherCurve
from OCC.Core.IFSelect import IFSelect_RetDone
from OCC.Core.STEPControl import STEPControl_Reader
from OCC.Core.GeomLProp import GeomLProp_SLProps
from OCC.Core.TopAbs import TopAbs_FORWARD, TopAbs_REVERSED
from OCC.Core.BRepTools import breptools
from OCC.Core.BRepAdaptor import BRepAdaptor_Curve, BRepAdaptor_Surface
from OCC.Core.GCPnts import GCPnts_AbscissaPoint, GCPnts_AbscissaPoint_Length
from OCC.Core.BRepBuilderAPI import BRepBuilderAPI_MakeVertex
from OCC.Core.BRepExtrema import BRepExtrema_DistShapeShape
from OCC.Core.ShapeFix import ShapeFix_Shape
from OCC.Core.BRepGProp import brepgprop
from OCC.Core.GProp import GProp_GProps
from OCC.Core.TopoDS import topods_Vertex, topods_Face, topods_Wire, topods_Edge
from OCC.Extend.TopologyUtils import TopologyExplorer
from OCC.Core.BRepMesh import BRepMesh_IncrementalMesh
from OCC.Core.BRepExtrema import BRepExtrema_ShapeProximity
from OCC.Core.TopoDS import topods_Face
from OCC.Core.gp import gp_Vec
from TopologyUtils import TopologyExplorer



#############################################################
####                      Topology                      #####
#############################################################


def ask_face_wires(face, topo):
    """
    This returns a list of wires(which is a loop of oriented edge). The first one is the boundary of the face.
    The followings (if has) are the inner loops.
    """
    wireList = []
    allWires = list(topo.wires_from_face(face))

    if len(allWires) == 1:
        return allWires
    elif len(allWires) > 1:
        f = topods_Face(face)
        outer = breptools.OuterWire(f)
        inners = [x for x in allWires if x != outer]
        wireList.append(outer)
        wireList.extend(inners)
        return wireList
    else:
        print("Function: face_wires, the number of wires of the face is 0 ")

        return None


def get_edges_bounded_by_vertex_in_face(topo, vertex, face):
    """
    """
    vertex_ds = topods_Vertex(vertex)
    face_ds = topods_Face(face)
    es1 = list(topo.edges_from_vertex(vertex_ds))
    es2 = list(topo.edges_from_face(face_ds))
    boundingEdge = PB.list_common(es1, es2)
    print(len(boundingEdge))
    if len(boundingEdge) != 2:
        print("The vertex is not bounded by two edges")

    return boundingEdge


def get_wire_of_edge_in_face(topo, edge, face):
    """
    """
    ws1 = list(topo.wires_from_edge(edge))
    ws2 = list(topo.wires_from_face(face))
    commonW = PB.list_common(ws1, ws2)

    return commonW[0]


def order_two_edges_in_wire(topo, wire, e1, e2):
    """
    """
    wire_ds = topods_Wire(wire)
    orderedE = list(topo.ordered_edges_from_wire(wire_ds))

    index1 = -1
    index2 = -1

    for i in range(len(orderedE)):
        if orderedE[i] == e1:
            index1 = i
            break
    for i in range(len(orderedE)):
        if orderedE[i] == e2:
            index2 = i
            break

    if index1 == 0 and index2 == len(orderedE) - 1:
        firstE = e2
        nextE = e1
    elif index2 == 0 and index1 == len(orderedE) - 1:
        firstE = e1
        nextE = e2
    elif index1 < index2:
        firstE = e1
        nextE = e2
    else:
        firstE = e2
        nextE = e1

    return [firstE, nextE]


#############################################################
####                      Geometry                      #####
#############################################################

def ask_vertex_parm_edge(vertex, edge):
    """
    Ask the parameter of an edge
    """
    vertex_ds = topods_Vertex(vertex)
    edge_ds = topods_Edge(edge)
    t = BRep_Tool().Parameter(vertex_ds, edge_ds)

    return t


def ask_vertex_uv_face(vertex, face):
    """
    This return the uv of a vertex on a face
    """
    gpPnt2D = BRep_Tool().Parameters(vertex, face)

    return gpPnt2D.Coord()


def ask_vertex_coord(shape):
    """
    Ask the coordinate of the vertex
    """
    # coord = BRep_Tool().Pnt(vertex).Coord()
    vertex = topods_Vertex(shape)
    pnt = BRep_Tool.Pnt(vertex)

    return [pnt.X(), pnt.Y(), pnt.Z()]


def ask_edge_midPnt(edge):
    """
    """
    result = BRep_Tool.Curve(edge)  # result[0] is the handle of curve;result[1] is the umin; result[2] is umax
    tmid = (result[1] + result[2]) / 2
    p = gp_Pnt(0, 0, 0)
    result[0].D0(tmid, p)

    return p.Coord()


def ask_edge_midpnt_tangent(edge):
    """
    Ask the midpoint of an edge and the tangent at the midpoint
    """
    result = BRep_Tool.Curve(edge)  # result[0] is the handle of curve;result[1] is the umin; result[2] is umax
    tmid = (result[1] + result[2]) / 2
    p = gp_Pnt(0, 0, 0)
    v1 = gp_Vec(0, 0, 0)
    result[0].D1(tmid, p, v1)  # handle.GetObject() gives Geom_Curve type, p:gp_Pnt, v1:gp_Vec

    return [p.Coord(), v1.Coord()]


def ask_edge_tangent(edge, parm):
    """
    parm is normalized parameter
    """
    result = BRep_Tool.Curve(edge)  # result[0] is the handle of curve;result[1] is the umin; result[2] is umax
    t = result[1] + (result[2] - result[1]) * parm
    p = gp_Pnt(0, 0, 0)
    v1 = gp_Vec(0, 0, 0)
    result[0].D1(t, p, v1)  # handle.GetObject() gives Geom_Curve type, p:gp_Pnt, v1:gp_Vec

    return v1.Coord()


def ask_edge_tangent2(edge, unNormParm):
    """
    """
    edge_ds = topods_Edge(edge)
    result = BRep_Tool.Curve(edge_ds)  # result[0] is the handle of curve;result[1] is the umin; result[2] is umax

    p = gp_Pnt(0, 0, 0)
    v1 = gp_Vec(0, 0, 0)
    result[0].D1(unNormParm, p, v1)  # handle.GetObject() gives Geom_Curve type, p:gp_Pnt, v1:gp_Vec

    return v1.Coord()


def ask_face_centroid(face):
    """
    """
    massProps = GProp_GProps()
    brepgprop.SurfaceProperties(face, massProps)
    gPt = massProps.CentreOfMass()

    return gPt.Coord()


def ask_point_uv(xyz, face, uvrange):
    """
    This is a general function which gives the uv coordinates from the xyz coordinates.
    The uv value is not normalised.
    """
    gpPnt = gp_Pnt(float(xyz[0]), float(xyz[1]), float(xyz[2]))
    face_ds = topods_Face(face)
    surface = BRep_Tool().Surface(face_ds)  # Handle_Geom_Surface

    sas = ShapeAnalysis_Surface(surface)
    gpPnt2D = sas.ValueOfUV(gpPnt, 0.01)
    uv = list(gpPnt2D.Coord())
    #    print(uvrange)
    #    print(uv)
    geom_surface = surface
    tol = 0.001
    if geom_surface.IsUPeriodic():
        #        print (geom_surface.UPeriod())
        if uv[0] < uvrange[0] and uvrange[0] - uv[0] > tol:
            uv[0] = uv[0] + geom_surface.UPeriod()
        if uv[0] > uvrange[1] and uv[0] - uvrange[1] > tol:
            uv[0] = uv[0] - geom_surface.UPeriod()

    if geom_surface.IsVPeriodic():
        if uv[1] < uvrange[2] and uvrange[2] - uv[1] > tol:
            uv[1] = uv[1] + geom_surface.VPeriod()
        if uv[1] > uvrange[3] and uv[1] - uvrange[3] > tol:
            uv[1] = uv[1] - geom_surface.VPeriod()
    #    print(uv)

    return uv


def ask_point_uv2(xyz, face):
    """
    This is a general function which gives the uv coordinates from the xyz coordinates.
    The uv value is not normalised.
    """
    gpPnt = gp_Pnt(float(xyz[0]), float(xyz[1]), float(xyz[2]))
    surface = BRep_Tool().Surface(face)  # Handle_Geom_Surface

    sas = ShapeAnalysis_Surface(surface)
    gpPnt2D = sas.ValueOfUV(gpPnt, 0.01)
    uv = list(gpPnt2D.Coord())

    return uv


def ask_point_normal_face(uv, face):
    """
    Ask the normal vector of a point given the uv coordinate of the point on a face
    """
    face_ds = topods_Face(face)
    surface = BRep_Tool().Surface(face_ds)
    props = GeomLProp_SLProps(surface, uv[0], uv[1], 1, 1e-6)
    #    GeomLProp_SLProps.SetParameters(surface,uv[0],uv[1])
    #    GeomLProp_SLProps.SetSurface(surface)

    gpDir = props.Normal()  # gp_Dir type
    if face.Orientation() == TopAbs_REVERSED:
        gpDir.Reverse()
        # print("face reversed")

    return gpDir.Coord()


def corner_angle(topo, vertex, face):
    """
    """
    angle = 0
    edges = get_edges_bounded_by_vertex_in_face(topo, vertex, face)
    if len(edges) == 0 or len(edges) == 1:  # it is possible that a vertex of a super face is only a vertex of
        # one of the hosts. In this case, for the other host faces, the angle should be 0
        # if len(edges) == 1:
        return angle
    wire = get_wire_of_edge_in_face(topo, edges[0], face)
    [firstE, nextE] = order_two_edges_in_wire(topo, wire, edges[0], edges[1])

    result1 = edge_extreme(firstE)
    result2 = edge_extreme(nextE)

    parm1 = ask_vertex_parm_edge(vertex, firstE)
    parm2 = ask_vertex_parm_edge(vertex, nextE)

    if abs(parm1 - result1[1]) < abs(parm1 - result1[2]):
        t1 = ask_edge_tangent2(firstE, result1[1])
    else:
        t1 = ask_edge_tangent2(firstE, result1[2])
        t1 = [x * -1 for x in t1]

    if abs(parm2 - result2[1]) < abs(parm2 - result2[2]):
        t2 = ask_edge_tangent2(nextE, result2[1])
    else:
        t2 = ask_edge_tangent2(nextE, result2[2])
        t2 = [x * -1 for x in t2]
    #    print(t1)
    #    print(t2)
    xyz = ask_vertex_coord(vertex)
    uv = ask_point_uv(xyz, face, face_extreme(face))
    n = ask_point_normal_face(uv, face)
    norm0 = np.sqrt(np.dot(t1, t1))
    norm1 = np.sqrt(np.dot(t2, t2))
    dp = np.dot(t1, t2)
    angle = np.arccos(round(dp / (norm0 * norm1), 4))
    #    print(angle)
    cp = np.cross(t2, t1)
    r = np.dot(cp, n)
    s = np.sign(r)
    if s == -1:
        angle = np.pi * 2 - angle
    else:
        pass
    #    print(angle)

    return angle


def arc_length_t_edge(edge, t1, t2):
    """
    This calculates the arc length between two parameters t1 and t2
    """
    adaptor3d_curve = BRepAdaptor_Curve(edge)
    arclength = GCPnts_AbscissaPoint_Length(adaptor3d_curve, t1, t2)

    return arclength


def edge_extreme(shape):
    """
    Get the parameter range of a curve underlying an edge
    """
    edge = topods_Edge(shape)
    (curve, tmin, tmax) = BRep_Tool.Curve(edge)

    return curve, tmin, tmax


def edge_dihedral(e, topo):
    """
    Calculate the dihedral angle of an edge
    """

    fs = list(topo.faces_from_edge(e))
    # print(len(fs))
    if len(fs) == 1:
        return 0
    for f in fs:
        edges = topo.edges_from_face(f)
        tem = None
        # orientation=0
        for ee in edges:
            if ee.IsEqual(e) or ee.IsSame(e):
                tem = ee
                break

        [midPnt, tangent] = ask_edge_midpnt_tangent(tem)
        uv0 = ask_point_uv2(midPnt, fs[0])
        uv1 = ask_point_uv2(midPnt, fs[1])
        n0 = ask_point_normal_face(uv0, fs[0])
        #    p0=xyz_from_uv_face(uv0,fs[0])
        n1 = ask_point_normal_face(uv1, fs[1])
        #    p1=xyz_from_uv_face(uv1,fs[1])

        angle = 0
        a = 0
        if tem.Orientation() == TopAbs_FORWARD:
            # print("forward")
            dp = np.dot(n0, n1)
            norm0 = np.sqrt(np.dot(n0, n0))
            norm1 = np.sqrt(np.dot(n1, n1))
            angle = np.arccos(round(dp / norm0 * norm1, 4))
            cp = np.cross(n0, n1)
            r = np.dot(cp, tangent)
            s = np.sign(r)
            if s == -1:
                a = "Concave"
                angle = np.pi + angle
            elif s == 1:
                a = "Convex"
                angle = np.pi - angle
            elif s == 0:
                angle = np.pi
                a = "Smooth"
        else:
            # print("reversed")
            dp = np.dot(n0, n1)
            norm0 = np.sqrt(np.dot(n0, n0))
            norm1 = np.sqrt(np.dot(n1, n1))
            angle = np.arccos(round(dp / norm0 * norm1, 4))
            cp = np.cross(n1, n0)
            r = np.dot(cp, tangent)
            s = np.sign(r)
            if s == -1:
                angle = np.pi + angle
                a = "Concave"
            elif s == 1:
                angle = np.pi - angle
                a = "Convex"
            elif s == 0:
                angle = np.pi
                a = "Smooth"
        #        print(angle)
        #        print("=============")
        return a


def face_extreme(face):
    """
    """
    face_ds = topods_Face(face)
    uv = breptools.UVBounds(face_ds)

    return uv


def min_distance(pt, face):
    """
    Minimum distance between point and face
    """

    gpPnt = gp_Pnt(pt[0], pt[1], pt[2])
    vertex = BRepBuilderAPI_MakeVertex(gpPnt).Vertex()
    dis = BRepExtrema_DistShapeShape(vertex, face)

    p = dis.PointOnShape2(1)
    d = dis.Value()

    return [d, p]


def make_vertex(pt):
    """
    create vertex at a point on the face
    """
    gpPnt = gp_Pnt(pt[0], pt[1], pt[2])
    vertex = BRepBuilderAPI_MakeVertex(gpPnt).Vertex()

    return vertex


def re_parameterization_arclength(edge, t1, tmin):
    """
    This function re-parameterize the curve using arc length as parameter.
    """
    #    (curve,tmin,tmax)=BRep_Tool.Curve(edge)
    partLength = arc_length_t_edge(edge, t1, tmin)
    wholeLength = GCPnts_AbscissaPoint().Length(BRepAdaptor_Curve(edge))

    return partLength / wholeLength


def xyz_from_t_edge(edge, parameter):
    """
    Given an edge and a normalized parameter[0,1], this calculates the point on the edge
    1. First extract the curve from edge and non-normalized parameter range tmin, tmax
    2. Translate the input parameter to non-normalized parameter
    3. Calculate the point (return a gpPnt)

    """

    result = BRep_Tool.Curve(edge)  # result[0] is the handle of curve;result[1] is the umin; result[2] is umax
    #    print(parameter)
    #    print(result[2])
    #    print(result[1])
    np = result[1] + (result[2] - result[1]) * parameter
    aPnt = result[0].Value(np)  # handle_Geom_Curve.GetObject() will give the Geom_Curve

    return aPnt


def xyz_from_arclength_edge(edge, arclength, tmin):
    """
    Given arc length from the endpoint with parameter tmin, return the xyz on edge
    """
    adaptor3d_Curve = BRepAdaptor_Curve(edge)
    abscissaPoint = GCPnts_AbscissaPoint(adaptor3d_Curve, arclength, tmin)

    result = BRep_Tool.Curve(edge)  # result[0] is the handle of curve;result[1] is the umin; result[2] is umax
    aPnt = result[0].Value(abscissaPoint.Parameter())

    return aPnt


def xyz_from_uv_face(uv, face):
    """
    Given a face and a uv coordinate, return the xyz coordinate
    """
    #    print(uv)
    [umin, umax, vmin, vmax] = face_extreme(face)
    u = umin + (umax - umin) * uv[0]
    v = vmin + (vmax - vmin) * uv[1]
    result = BRep_Tool.Surface(face)  # result[0] is the handle of curve;result[1] is the umin; result[2] is umax
    aPnt = result.Value(u, v)  # handle_Geom_Curve.GetObject() will give the Geom_Curve

    return aPnt


def xyz_from_uv_face_unnormlized(uv, face):
    """
    Given a face and a uv coordinate, return the xyz coordinate
    """
    #    print(uv)
    result = BRep_Tool.Surface(face)  # result[0] is the handle of curve;result[1] is the umin; result[2] is umax
    aPnt = result.Value(uv[0], uv[1])  # handle_Geom_Curve.GetObject() will give the Geom_Curve

    return aPnt


def fix_shape(shape):
    """
    """
    sfs = ShapeFix_Shape(shape)
    #    sfs.Init(shape)
    sfs.SetPrecision(0.01)
    # sfs.FixEdgeTool
    #    sfs.SetMaxTolerance(0.02)
    #    w=sfs.FixWireTool().GetObject()
    #    l=w.FixDegenerated()
    #    t=w.FixSmall(True,0.01)
    #    print(t)
    ##    sfs.SetMinTolerance(0.001)
    sfs.Perform()
    result = sfs.Shape()

    #    sfwf=ShapeFix_Wireframe(shape)
    #    sfwf.SetModeDropSmallEdges=True
    #    print("max tolerance: %f"%sfwf.MaxTolerance())
    #    print("min tolerance: %f"%sfwf.MinTolerance())
    #    print("Precision: %f"%sfwf.Precision())
    #    sfwf.SetPrecision(0.001)
    #    sfwf.SetMaxTolerance(0.002)
    #    print("max tolerance: %f"%sfwf.MaxTolerance())
    #    print("min tolerance: %f"%sfwf.MinTolerance())
    #    print("Precision: %f"%sfwf.Precision())
    #    sfwf.FixSmallEdges()
    #
    #
    #    result=sfwf.Shape()

    return result


def normalize_node_edge_parameter(nodeInfo, topo):
    """
    This replaces the non-normalized parameter of a node on an edge to the normalized parameter.

    """
    edges = list(topo.edges())
    # print(len(edges))
    for i in range(0, len(edges)):
        (curve, tmin, tmax) = edge_extreme(edges[i])

        for label in list(nodeInfo[2].values())[i]:
            np = (float(nodeInfo[3][label][3]) - tmin) / (tmax - tmin)  # calculate normalized parameter
            nodeInfo[3][label][3] = np


def normalize_node_face_parameter(nodeInfo, topo):
    """
    This replaces the non-normalized parameter of a node on an edge to the normalized parameter.

    """
    faces = list(topo.faces())
    for i in range(0, len(faces)):
        [umin, umax, vmin, vmax] = face_extreme(faces[i])

        for label in list(nodeInfo[4].values())[i]:
            normU = (float(nodeInfo[5][label][3]) - umin) / (umax - umin)  # calculate normalized parameter
            nodeInfo[5][label][3] = normU
            normV = (float(nodeInfo[5][label][4]) - vmin) / (vmax - vmin)  # calculate normalized parameter
            nodeInfo[5][label][4] = normV


def read_step_file(filename):
    """ read the STEP file and returns a compound
    """
    step_reader = STEPControl_Reader()
    status = step_reader.ReadFile(filename)

    if status == IFSelect_RetDone:  # check status
        step_reader.TransferRoot(1)
        a_shape = step_reader.Shape(1)
    else:
        print("Error: can't read file.")
        sys.exit(0)

    return a_shape


def recognise_face_type(face):
    """
    Takes a TopoDs shape and determines the nature of the the shapes surface type, i.e. planar, cylindrical...
    """
    #   BRepAdaptor to get the face surface, GetType() to get the type of geometrical surface type
    surf = BRepAdaptor_Surface(face, True)
    surf_type = surf.GetType()
    a = 0
    if surf_type == GeomAbs_Plane:
        a = 1

    elif surf_type == GeomAbs_Cylinder:
        a = 2

    elif surf_type == GeomAbs_Torus:
        a = 3

    elif surf_type == GeomAbs_Sphere:
        a = 4

    elif surf_type == GeomAbs_Cone:
        a = 5

    elif surf_type == GeomAbs_BezierSurface:
        a = 6

    elif surf_type == GeomAbs_BSplineSurface:
        a = 7

    elif surf_type == GeomAbs_SurfaceOfRevolution:
        a = 8

    elif surf_type == GeomAbs_OffsetSurface:
        a = 9

    elif surf_type == GeomAbs_SurfaceOfExtrusion:
        a = 10

    elif surf_type == GeomAbs_OtherSurface:
        a = 11

    return a


def recognise_edge_type(edge):
    edge = BRepAdaptor_Curve(edge)
    edge_type = edge.GetType()

    if edge_type == GeomAbs_Line:
        b = 1

    elif edge_type == GeomAbs_Circle:
        b = 2

    elif edge_type == GeomAbs_OtherCurve:
        b = 3

    elif edge_type == GeomAbs_Hyperbola:
        b = 4

    elif edge_type == GeomAbs_Ellipse:
        b = 5

    elif edge_type == GeomAbs_Parabola:
        b = 6

    elif edge_type == GeomAbs_BezierCurve:
        b = 7

    elif edge_type == GeomAbs_BSplineCurve:
        b = 8

    elif edge_type == GeomAbs_OtherCurve:
        b = 9

    else:
        b = 0

    return b


def edge_length(edge):
    #    result=BRep_Tool.Curve(edge)###result[0] is the handle of curve;result[1] is the umin; result[2] is umax
    #    """
    """
    Calculate the length of an edge
    """
    length = GCPnts_AbscissaPoint().Length(BRepAdaptor_Curve(edge))
    b = length

    return b


def edge_length_ratios(topo):
    #    result=BRep_Tool.Curve(edge)###result[0] is the handle of curve;result[1] is the umin; result[2] is umax
    #    """
    """
    Calculate the length of an edge
    """
    global edge_length_ratio
    length_list = []
    fs = list(topo.faces())
    # print(len(fs))
    for f in fs:
        edges = list(topo.edges_from_face(f))
        for ee in edges:
            length = GCPnts_AbscissaPoint().Length(BRepAdaptor_Curve(ee))
            length_list.append(length)

        max_length = max(length_list)
        min_length = min(length_list)
        edge_length_ratio = round((max_length - min_length) / ((max_length + min_length) / 2), 5)
        print(edge_length_ratio)
        length_list.clear()

    return edge_length_ratio


def surface_area(f):
    """ Compute the surface of each face of a shape
    """
    props = GProp_GProps()

    brepgprop_SurfaceProperties(f, props)
    area = props.Mass()
    return area

def solidvolume(s):
    """ Compute the surface of each face of a shape
    """

    props = GProp_GProps()

    brepgprop_VolumeProperties(s, props)
    volume = props.Mass()
    return volume

def solid_surface_area(s):
    """ Compute the surface of each face of a shape
    """

    props = GProp_GProps()

    brepgprop_SurfaceProperties(s, props)
    area = props.Mass()
    return area


def recognize_batch():
    """
    Menu item : process all the faces of a single shape
    """
    #   then traverse the topology using the Topo class
    t = TopologyExplorer(step_shape)

    #   loop over faces only
    for f in t.faces():
        #   call the recognition function
        recognise_face_type(f)

    for e in t.edges():
        edge_length(e)


def collision_check(ss, i, cnt, fHash_ID, tol=1e-5):
    if len(ss) > 1:
        shape_a = i
        shape_b = cnt

        BRepMesh_IncrementalMesh(shape_a, 1e-3).Perform()
        BRepMesh_IncrementalMesh(shape_b, 1e-3).Perform()
        proximity = BRepExtrema_ShapeProximity(shape_a, shape_b, tol)
        proximity.Perform()
        distance = BRepExtrema_DistShapeShape()
        distance.LoadS1(shape_a)
        distance.LoadS2(shape_b)
        distance.Perform()

        assert distance.IsDone()

        mindistance = distance.Value()

        if proximity.IsDone():
            # Get intersecting faces from Shape1
            overlaps1 = proximity.OverlapSubShapes1()
            face_indices1 = overlaps1.Keys()
            shape_a_faces = []
            shape_a_hash_faces = []
            for inda in face_indices1:
                facea = proximity.GetSubShape1(inda)
                shape_a_faces.append(np.int(fHash_ID[hash(facea)]))
                shape_a_hash_faces.append(facea)

            # Get intersecting faces from Shape2
            overlaps2 = proximity.OverlapSubShapes2()
            face_indices2 = overlaps2.Keys()
            shape_b_faces = []
            shape_b_hash_faces = []
            for indb in face_indices2:
                faceb = proximity.GetSubShape2(indb)
                shape_b_faces.append(np.int(fHash_ID[hash(faceb)]))
                shape_b_hash_faces.append(faceb)

            if 0 - tol <= mindistance <= 0 + tol:
                coll_type = 1
            elif mindistance > 0 + tol:
                coll_type = 3
            else:
                coll_type = 2

        else:
            coll_type = 0
            shape_a_faces = None
            shape_b_faces = None
            shape_a_hash_faces = None
            shape_b_hash_faces = None

        return coll_type, shape_a_faces, shape_b_faces, shape_a_hash_faces, shape_b_hash_faces, mindistance


"""
    display, start_display, add_menu, add_function_to_menu = init_display()
    display.SetSelectionModeVertex()
    display.EraseAll()
    display.DisplayShape(shape, update=True, transparency=0.2)
    start_display()
"""

if __name__ == "__main__":
    sg.theme('Reddit')
    col = [[sg.Image("QUB-logo.png")]]
    but = [[sg.Button("Run", button_color=("white", "green"), size=(10, 1))]]

    layout = [
        [sg.Column(col, vertical_alignment='Centre', justification='Centre')],
        [sg.Text("Choose a File: ", justification="centre"), sg.Input("", key="in", size=(55, 10)),
         sg.FileBrowse(file_types=(("Step Files", "*.Step"),))],
        [sg.Column(but, vertical_alignment='Centre', justification='Centre')]]
    #    [sg.Output(size=(79, 40))]]

    window = sg.Window('File Explorer', layout, size=(600, 250))

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break
        elif event == "Run":
            file_path = values["in"]
            path = values["in"]
            modelName = os.path.basename(path)
            shape = read_step_file(path)
            topo = TopologyExplorer(shape)
            step_shape = read_step_file(path)

            recognize_batch()
            ss = topo.solids()


