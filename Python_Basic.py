# -*- coding: utf-8 -*-
"""
Created on Thu Apr 19 19:57:55 2018

Author name: Liang Sun
@author: 3052180
Queen's University Belfast
"""

import triangle
import numpy as np
import math

import subprocess
import sys
import os
from numpy.linalg import inv
import logging

#############################################################
####               Dictionsary and list                 #####
#############################################################

def dict_ask_key_by_value(dictionary,queryValue):
    """
    Get the key of a dictionary from the value
    """
    returnKey=None
    for key, value in dictionary.items():
        if value == queryValue:
            returnKey=key
            break
    return returnKey



def distance(p1,p2):
    """
    Distance between two points
    """

    return math.sqrt(sum((p1[i]-p2[i])*(p1[i]-p2[i]) for i in range(0,3)))



def list_common(list1, list2):
    """
    Return elements in list1 that are also in list2
    """
    common=[]
    for x in list1:
        if x in list2:
            common.append(x)
    return common

def list_rearrange(aList, element):
    """
    This is to rearrange a list, to make the input element the first in the list
    """
    index=aList.index(element)
    newList=[]
    for i in range(index,len(aList)):
        newList.append(aList[i])
    for i in range(0,index):
        newList.append(aList[i])
    return newList


def list_remove_list(list1, list2):
    """
    Remove list2 from list1
    """
    for x in list2:
        if x in list1:
            list1.remove(x)

    return list1


def list_find_repeat_element(aList):
    """
    Return the index of duplicated elements in a list
    """
    indices=[]
    for i in range(0,len(aList)-1):
        for j in range(i+1,len(aList)):
            if aList[i]==aList[j]:
                indices.append(i)
                indices.append(j)
    indices=list(set(indices))
    return indices


#############################################################
####                    Vectors                         #####
#############################################################



def vector_add(u, v):
    """
    Add two vectors 
    """
    return [ u[i]+v[i] for i in range(len(u)) ]


def vector_cross_product(v1,v2):
    """
    Cross product
    """
    return np.cross(v1,v2)


def vector_dot(u, v):
    """
    Dot product
    """
    return sum(u[i]*v[i] for i in range(len(u)))


def vector_magnitude(v):
    """
    Magnitude
    """
    return math.sqrt(sum(v[i]*v[i] for i in range(len(v))))

def vector_normalize(v):
    """
    Normalize a vector
    """
    vmag = vector_magnitude(v)
    return [ v[i]/vmag  for i in range(len(v)) ]

def vector_round(v):
    """
    Round each element in a vector
    """
    return[round(v[i], 2) for i in range(len(v))]

def vector_scale(v,scaler):
    """
    Scale a vector
    """
    return [x*scaler for x in v]

def vector_sub(u, v):
    """
    Calculate difference between two vectors
    """
    return [ u[i]-v[i] for i in range(len(u)) ]



#############################################################
####                    I/O                             #####
#############################################################


def init_logging_wrt(file):
    """
    Write a log file. See logging mudule in python
    """
    
    # set up logging to file - see previous section for more details
    logging.getLogger().handlers.clear()
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(levelname)-6s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename=file,
                        filemode='w')

    logging.error("=============================================================")
    logging.error("Log created by Liang Sun, 3052180")
    logging.error("============================================================="+"\n")



    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)



def wrap_list(lst, items_per_line=5):
    """
    when writing or print a long list, sometime we want to make it more
    organised. This wrip a list so each line only has the specified number 
    of entities.
    """
    lines = []
    for i in range(0, len(lst), items_per_line):
        chunk = lst[i:i + items_per_line]
        line = "    ".join("{:.9e}".format(x) for x in chunk)
        lines.append(line)
    return "\n".join(lines) 



def write_file(path, fileName, writeType, content):
    """
    Write file
    """
    filePath=path+fileName
    if writeType=="w":
        if os.path.exists(filePath):
            os.remove(filePath)
    
    file= open(filePath, writeType )
    file.write(content)

    file.close()



def read_file(path, fileName, readType):
    """
    Read the first line
    """
    
    file= open(path +"/"+ fileName, readType )
    content=file.readlines()[0]
    file.close()
    return content







def python3_call_python2(python2, module,argv):
    """
    Call python2 script in python3 by using subprocess.
    Parameters:
        python2: location of python2.exe
        module: python2 script, e.g. test.py
        argv: arguments passed into the module file. 
    Return: 
        all output from the "print" function in module file. Note all element will be string
    """
#    allOutput=[]
#    process = subprocess.Popen([python2, module]+ argv,shell = False, stdout=subprocess.PIPE,encoding='utf8')
#    aa=process.communicate()
#    print("=====")
#    print(aa)
#    print("sssssss")

#    while True:
#        output = process.stdout.readline()
#        if output == '' and process.poll() is not None:
#            break
#        if output:
#            #print (output.strip())
#            allOutput.append(output.strip())

#    return allOutput
    sb=subprocess.Popen([python2, module]+ argv,shell = False, stdout=None)
    sb.communicate()


#############################################################
####                 Geometry operation                 #####
#############################################################
def ptInTriang(p_test, p0, p1, p2):
    """
    Find if a point is in a triangle.
    Parameters:
        p_test: test point
        p0, p1, p2: points of triangle
    Return:
        if point is inside and its barycentri coordinate (r,s ,t)
        the first element: if a point is inside. True for in and false for not
        s=s_p/D, t=t_p/D, r=1-s-t 
    """
#    print(p_test)
#    print(p0)
    dX = p_test[0] - p0[0]
    dY = p_test[1] - p0[1]
    dX20 = p2[0] - p0[0]
    dY20 = p2[1] - p0[1]
    dX10 = p1[0] - p0[0]
    dY10 = p1[1] - p0[1]

    s_p = (dY20*dX) - (dX20*dY)
    t_p = (dX10*dY) - (dY10*dX)
    D = (dX10*dY20) - (dY10*dX20)
    s,t=s_p/D,t_p/D
    r=1-s-t

    if D > 0:
        return ( ( (s_p >= 0) and (t_p >= 0) and (s_p + t_p) <= D), r,s,t )
    else:
        return ( ( (s_p <= 0) and (t_p <= 0) and (s_p + t_p) >= D), r,s,t )






def node_triangles(triangles):
    """
    Parameters:
        triangles: a list of triangles. Each triangle contains three labels representing the three node 
    Return:
        node_triangles:a dictionary storing a node and the triangles conneted to it
    """
    node_triangles={}
    for i in range(0, len(triangles)):
        for node in triangles[i]:
            if node in node_triangles:
                node_triangles[node].append(i)
            else:
                node_triangles[node]=[i]
    return node_triangles



def locate_point_in_trangles(triangles, nodes, node_triangles, testPnt, nearestNodeIndex):
    """
    This function returns which triangle in the input trianguation that the testPnt lies in and 
    its barycentric coordinates in that triangle.
    Parameters:
        triangles: a list of triangles. Each triangle contains the labels of three nodes.
                   Here the label is the index of a node in the "nodes" list
        node_triangles: a dictionary storing the connected triangles to each node
        nodes: a list of 2D nodes. Each 2D node is represented by [u,v]
        testPnt: a 2D point to be test
        nearestNodeIndex: the index of the nearest node in the "nodes" list to the testPnt. This can be achieved using the kdtree.
    Return:
        irst: irst=[i,r,s,t]. i: the index of the triangle that the testPnt lies in; s,t: two components
             of the barycentric coordinate rst. r+s+t=1
    """
    irst=[]
    ### Get index of one triangle conneted to the nearest point
    triIndex=node_triangles[nearestNodeIndex][0]


    while True:
        testTri=triangles[triIndex]
        [isIn,r,s,t]=ptInTriang(testPnt,nodes[testTri[0]],nodes[testTri[1]],nodes[testTri[2]])

        if isIn == True:
            irst=[triIndex,r,s,t]
            #print("found")
            break
        else:
            m,n=0,0
            if s<0:m,n=0,2
            if t<0:m,n=0,1
            if s+t>1:m,n=1,2

            common=list_common(node_triangles[testTri[m]],node_triangles[testTri[n]])
            common.remove(triIndex)
#            print("common ", common)
            if len(common)<1:
                #print("brute force")
                irst=locate_point_in_triangles_bruteforce(testPnt, triangles, nodes )
                break
            else:
                triIndex=common[0]
    return irst



def locate_point_in_triangles_bruteforce(testPnt, triangles, nodes ):
    """
    Find which triangle the input node lies in.
    return the index of the triangle and its barycentric coordiantes in the triangle
    Parameters:
        testPnt: the coordinate of a 2D node to be tested
        triangles: a list of triangles. Each triangle store a list of labels of the triangle nodes.
        nodes: a list of nodes
    
    Return: 
        i: which trianle it lies in
        s, t two value of the barycentric coordinate (r,s,t) r=1-s-t
    """
    irst=[]
    for i in range (0, len(triangles)):
        [isIn,r,s,t]=ptInTriang(testPnt,nodes[triangles[i][0]],nodes[triangles[i][1]],nodes[triangles[i][2]])
#        [isIn,r,s,t]=ptInTriang2(np.array(testPnt),nodes[triangles[i]])
        if isIn == True:
            irst=[i,r,s,t]
            return irst



def ask_point_in_polygon(polygon):
    """
    Ask a point inside a polygon. Do constrained delauney triangulation of the polygon and find 
    the centriod of one triangle. See virtual_parameterization.py
    Parameters:
        polygon: a list of points forming a closed 2D polygon
    Return:
        a point inside the polygon
    """
    ### Get segments
    segmts=[]
    for j in range(0,len(polygon)-1):
        segmts.append([j,j+1])
    segmts.append([len(polygon)-1,0])
    
    ###Constraint delaunay triangulation
    test= dict(vertices=polygon,segments=segmts)
    t=triangle.triangulate(test,'p')
    
    ###Calulate centriod of one triangle
    tri=t['triangles'][0]
    nodes=t['vertices'][tri]
    return np.sum(nodes, axis=0)/3


def point_in_triangles(irst,triangles,vertices):
    """
    Given a triangulation and irst, calculate the point.
    Properties:
        irst: i: index of triangle, rst:barycentric coordinate
    """
    tri=triangles[irst[0]] ### Triangle
    rst=np.array(irst[1:]) 
    return rst.dot(vertices[tri])### vertices[tri] are coordinates of triangel nodes














