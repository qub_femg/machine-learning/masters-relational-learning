from __future__ import print_function

import os.path
import sys
import PySimpleGUI as sg

from OCC.Core.STEPControl import STEPControl_Reader
from OCC.Core.IFSelect import IFSelect_RetDone, IFSelect_ItemsByEntity
from OCC.Display.SimpleGui import init_display
from OCC.Extend.DataExchange import read_step_file

from OCC.Extend.TopologyUtils import TopologyExplorer

if __name__ == '__main__':
    sg.theme('Reddit')
    col = [[sg.Image("QUB-logo.png")]]
    but = [[sg.Button("Run", button_color=("white", "green"), size=(10, 1))]]

    layout = [
        [sg.Column(col, vertical_alignment='Centre', justification='Centre')],
        [sg.Text("Choose a File: ", justification="centre"), sg.Input("", key="in", size=(55, 10)),
         sg.FileBrowse(file_types=(("Step Files", "*.Step"),))],
        [sg.Column(but, vertical_alignment='Centre', justification='Centre')]]
    #    [sg.Output(size=(79, 40))]]

    window = sg.Window('File Explorer', layout, size=(600, 250))

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break
        elif event == "Run":
            file_path = values["in"]
            path = values["in"]
            modelName = os.path.basename(path)
            shape = read_step_file(path)
            topo = TopologyExplorer(shape)
            step_shape = read_step_file(path)

            display, start_display, add_menu, add_function_to_menu = init_display()
            display.SetSelectionModeFace()  # switch to Face selection mode
            # first loads the STEP file and display
            display.DisplayShape(step_shape, update=True, color='black')
            start_display()





A, R, _, _, _ = rescal.als([x0, x1, x2, x3, x5, x6, x7, x9], 3, compute_fit=True)