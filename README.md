# Master's Project - Relational Learning
This is a repo containing the code for a final year MEng master's project completed by David Philpott. It is a open-source implementation of a relational learning approach originally purposed by Flavien Boussuge et al in the paper: [Application of Tensor Factorisation to Analyse Similarities in CAD Assembly Models](#https://www.sciencedirect.com/science/article/pii/S0010448522001178).

## Abstract
Relational learning is a machine learning-based approach that extracts and processes data to identify the relationship between data sets. Companies spend a lot of money and time modifying geometric models to be analysed with a Finite Element Analysis (FEA) package. Using relational learning allows for the identification of repeated, missing, or inconsistent data within a model. One of the applications of Relational Learning in CAD is to prepare the model for analysis by automatically applying boundary conditions. Within CAD-based applications, relational learning can identify repeated features like bolts and allow the user to remove these for the simulation, reducing the model complexity. It is possible to save time and money by automating the pre-processing phase of a model for FEA. This work is to investigate the use of relational learning and determine its value within everyday engineering.

<div align="center">
  <img src=masters_project_gear_teeth.JPG width="50%"></img>
</div>

## Requirements
Requirements and how to run the application can be found in the attached `Installation_Guide.pdf`.

## Citation
Please cite this work if used in your research:

    @article{MastersRelationalLearning2020,
      Author = {David Philpott, Andrew R. Colligan & Trevor T. Robinson},
      Title = {Using Relational Learning for Finite Element Analysis Boundary Condition Application},
      Year = {2020}
    }

    @article{CADTensorFactorisation2022,
      Author = {Flavien Boussuge, Cecil G. Armstrong, Christopher M. Tierney & Trevor T. Robinson},
      Journal = {Computer-Aided Design},
      Title = {Application of tensor factorisation for CAE model preparation from CAD assembly models},
      Year = {2022}
      Volume = {152}
      DOI = {https://doi.org/10.1016/j.cad.2022.103372}
    }
