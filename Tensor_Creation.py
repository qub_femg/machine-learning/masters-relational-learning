from scipy.sparse import csr_matrix
import pandas as pd
import numpy as np
import scipy as sc
import rescal
import csv


def slice_k0_creation(ent, topo):
    """k0 contains the adjacency relationship between faces, edges and vertices of the B-Rep model."""
    n = len(ent['ID'])

    ent_list = topo['Entity']
    bnd_ent_list = topo['BoundEntity']

    slice_k0 = np.zeros([n, n])

    cnt = 0
    for b in ent_list:
        if b != 0:
            slice_k0[b - 1, bnd_ent_list[cnt] - 1] = 1

        cnt = cnt + 1

    # keeps data in a spare form where only a present relationship is recorded (only the 1 values in matrix)
    x0 = sc.sparse.csr_matrix(slice_k0)

    return x0


def slice_k1_creation(ent):
    """k1 contains the surface type information defining each face"""
    n = len(ent['ID'])
    m = 11   # taken from the OCCD_Basic function recognise_face_type

    geom_type_list = ent['GeomType']

    slice_k1 = np.zeros([n, n])

    cnt = 0
    for row in slice_k1:
        if geom_type_list[cnt] != 0:
            row[geom_type_list[cnt] - 1] = 1

        cnt = cnt + 1

    # keeps data in a spare form where only a present relationship is recorded (only the 1 values in matrix)
    x1 = sc.sparse.csr_matrix(slice_k1)

    return x1


def slice_k2_creation(ent):
    """k2 contains the edge type information defining each edge
    """
    n = len(ent['ID'])
    m = 8    # taken from the OCCD_Basic function recognise_edge_type

    edge_type_list = ent['EdgeType']

    slice_k2 = np.zeros([n, n])

    cnt = 0
    for row in slice_k2:
        if edge_type_list[cnt] != 0:
            row[edge_type_list[cnt] - 1] = 1

        cnt = cnt + 1

    # keeps data in a spare form where only a present relationship is recorded (only the 1 values in matrix)
    x2 = sc.sparse.csr_matrix(slice_k2)
    return x2


def slice_k3_creation(ent):
    """k3 contains the convexity type information defining each edge
    """
    n = len(ent['ID'])
    m = 3    # taken from the OCCD_Basic function recognise_convexity_type (convex, concave, smooth)

    convexity_list = ent['EdgeConvexity']

    slice_k3 = np.zeros([n, n])

    cnt = 0
    for row in slice_k3:
        if convexity_list[cnt] != 0:
            row[convexity_list[cnt] - 1] = 1

        cnt = cnt + 1

    # keeps data in a spare form where only a present relationship is recorded (only the 1 values in matrix)
    x3 = sc.sparse.csr_matrix(slice_k3)

    return x3


def slice_k4_creation(ent):

    n = len(ent['ID'])

    shape_type_list = ent['ShapeType']

    slice_k4 = np.zeros([n, n])

    cnt = 0
    for row in slice_k4:
        row[shape_type_list[cnt] - 1] = 1

        cnt = cnt + 1

    # keeps data in a spare form where only a present relationship is recorded (only the 1 values in matrix)
    x4 = sc.sparse.csr_matrix(slice_k4)

    return x4


def slice_k5_creation(ent):

    n = len(ent['ID'])
    m = 25  # taken from the OCCD_Basic function recognise_convexity_type (convex, concave, smooth)

    ratio_list = ent['DeltaEdgeLengthRatio']

    slice_k5 = np.zeros([n, n])

    cnt = 0
    for r in slice_k5:
        if ratio_list[cnt] == 0:
            r[0] = 1
        elif 0 < ratio_list[cnt] <= 0.2:
            r[1] = 1
        elif 0.2 < ratio_list[cnt] <= 0.4:
            r[2] = 1
        elif 0.4 < ratio_list[cnt] <= 0.6:
            r[3] = 1
        elif 0.6 < ratio_list[cnt] <= 0.8:
            r[4] = 1
        elif 0.8 < ratio_list[cnt] <= 1.0:
            r[5] = 1
        elif 1.0 < ratio_list[cnt] <= 1.2:
            r[6] = 1
        elif 1.2 < ratio_list[cnt] <= 1.4:
            r[7] = 1
        elif 1.4 < ratio_list[cnt] <= 1.6:
            r[8] = 1
        elif 1.6 < ratio_list[cnt] <= 1.8:
            r[9] = 1
        elif 1.8 < ratio_list[cnt] <= 2.0:
            r[10] = 1
        elif 2.0 < ratio_list[cnt] <= 2.2:
            r[11] = 1
        elif 2.2 < ratio_list[cnt] <= 2.4:
            r[12] = 1
        elif 2.4 < ratio_list[cnt] <= 2.6:
            r[13] = 1
        elif 2.6 < ratio_list[cnt] <= 2.8:
            r[14] = 1
        elif 2.8 < ratio_list[cnt] <= 3.0:
            r[15] = 1
        elif 3.0 < ratio_list[cnt] <= 3.2:
            r[16] = 1
        elif 3.2 < ratio_list[cnt] <= 3.4:
            r[17] = 1
        elif 3.4 < ratio_list[cnt] <= 3.6:
            r[18] = 1
        elif 3.6 < ratio_list[cnt] <= 3.8:
            r[19] = 1
        elif 3.8 < ratio_list[cnt] <= 4.0:
            r[20] = 1
        elif 4.0 < ratio_list[cnt] <= 4.2:
            r[21] = 1
        elif 4.2 < ratio_list[cnt] <= 4.4:
            r[22] = 1
        elif 4.4 < ratio_list[cnt] <= 4.6:
            r[23] = 1
        elif 4.6 < ratio_list[cnt] <= 4.8:
            r[24] = 1
        elif 4.8 < ratio_list[cnt] <= 5.0:
            r[25] = 1
        elif 4.8 < ratio_list[cnt] > 5.0:
            r[26] = 1

        cnt = cnt + 1

    # keeps data in a spare form where only a present relationship is recorded (only the 1 values in matrix)
    x5 = sc.sparse.csr_matrix(slice_k5)

    return x5


def slice_k6_creation(ent):
    """k6 contains the number of bound entities contained in each entity"""
    n = len(ent['ID'])

    ent_list = ent['ID']
    no_bnd_ent = ent['No.BoundEntities']

    slice_k6 = np.zeros([n, n])

    cnt = 0
    for e in no_bnd_ent:
        if e != 0:
            slice_k6[ent_list[cnt] - 1, e - 1] = 1

        cnt = cnt + 1

    # keeps data in a spare form where only a present relationship is recorded (only the 1 values in matrix)
    x6 = sc.sparse.csr_matrix(slice_k6)

    return x6


def slice_k7_creation(ent, interference):
    """k7 contains the interference between solids"""
    n = len(ent['ID'])

    sld_a = interference['Entity']
    sld_b = interference['ContactEntity']

    slice_k7 = np.zeros([n, n])

    cnt = 0
    for f in sld_a:
        slice_k7[f - 1, sld_b[cnt] - 1] = 1

        cnt = cnt + 1

    # keeps data in a spare form where only a present relationship is recorded (only the 1 values in matrix)
    x7 = sc.sparse.csr_matrix(slice_k7)

    return x7


def slice_k8_creation(ent):

    n = len(ent['ID'])
    m = 25  # taken from the OCCD_Basic function recognise_convexity_type (convex, concave, smooth)

    ratio_list = ent['AreaPerimeterRatio']

    slice_k8 = np.zeros([n, n])

    cnt = 0
    for r in slice_k8:
        if 0 < ratio_list[cnt] <= 0.5:
            r[1] = 1
        elif 0.5 < ratio_list[cnt] <= 1:
            r[2] = 1
        elif 1 < ratio_list[cnt] <= 1.5:
            r[3] = 1
        elif 1.5 < ratio_list[cnt] <= 2:
            r[4] = 1
        elif 2 < ratio_list[cnt] <= 2.5:
            r[5] = 1
        elif 2.5 < ratio_list[cnt] <= 3:
            r[6] = 1
        elif 3 < ratio_list[cnt] <= 3.5:
            r[7] = 1
        elif 3.5 < ratio_list[cnt] <= 4:
            r[8] = 1
        elif 4 < ratio_list[cnt] <= 4.5:
            r[9] = 1
        elif 4.5 < ratio_list[cnt] <= 5:
            r[10] = 1
        elif 5 < ratio_list[cnt] <= 5.5:
            r[11] = 1
        elif 5.5 < ratio_list[cnt] <= 6:
            r[12] = 1
        elif 6 < ratio_list[cnt] <= 6.5:
            r[13] = 1
        elif 6.5 < ratio_list[cnt] <= 7:
            r[14] = 1
        elif 7 < ratio_list[cnt] <= 7.5:
            r[15] = 1
        elif 7.5 < ratio_list[cnt] <= 8:
            r[16] = 1
        elif 8 < ratio_list[cnt] <= 8.5:
            r[17] = 1
        elif 8.5 < ratio_list[cnt] <= 9:
            r[18] = 1
        elif 9 < ratio_list[cnt] <= 9.5:
            r[19] = 1
        elif 9.5 < ratio_list[cnt] <= 10:
            r[20] = 1
        elif 10 < ratio_list[cnt] <= 10.5:
            r[21] = 1
        elif 10.5 < ratio_list[cnt] <= 11:
            r[22] = 1
        elif 11 < ratio_list[cnt] <= 11.5:
            r[23] = 1
        elif 11.5 < ratio_list[cnt] <= 12:
            r[24] = 1
        elif 12 < ratio_list[cnt] <= 12.5:
            r[25] = 1
        elif ratio_list[cnt] > 12.5:
            r[26] = 1

        cnt = cnt + 1

    # keeps data in a spare form where only a present relationship is recorded (only the 1 values in matrix)
    x8 = sc.sparse.csr_matrix(slice_k8)

    return x8

def slice_k9_creation(ent, interference):
    """k9 contains the adjacency relationship between faces, edges and vertices of the B-Rep model."""
    n = len(ent['ID'])

    collision_ent_list = interference['Entity']
    contact_type = interference['InterferenceType']


    slice_k9 = np.zeros([n, n])

    cnt = 0
    for b in collision_ent_list:
        slice_k9[b-1, contact_type[cnt] - 1] = 1

        cnt = cnt + 1

    x9 = sc.sparse.csr_matrix(slice_k9)

    return x9


def run_rescal(path, modelName):
    ent = pd.read_csv(path + f'{modelName} - Entity List.csv')
    topo = pd.read_csv(path + f'{modelName} - Topology List.csv')
    interference = pd.read_csv(path + f'{modelName} - Interference List.csv')

    x0 = slice_k0_creation(ent, topo)
    x1 = slice_k1_creation(ent)
    x2 = slice_k2_creation(ent)
    x3 = slice_k3_creation(ent)
    x4 = slice_k4_creation(ent)
    x5 = slice_k5_creation(ent)
    x6 = slice_k6_creation(ent)
    x7 = slice_k7_creation(ent, interference)
    x8 = slice_k8_creation(ent)
    x9 = slice_k9_creation(ent, interference)

    A, R, _, _, _ = rescal.als([x0, x1, x2, x3, x4, x5, x6, x7, x8, x9], 3, compute_fit=True)

    print(A)
    return A


def compare_entities(a_i, a_j, delta):
    above_threshold = False
    k = np.exp(-np.power(np.linalg.norm(a_i - a_j), 2) / delta)

    if k > 1e-5:
        above_threshold = True

    return above_threshold, k


if __name__ == "__main__":
    path = "D:/User/OneDriver University/OneDrive - Queen\'s University Belfast/University/Stage 4/Project/Data Extraction from CAD/CSV Files/"
    csv_path = "D:/User/OneDriver University/OneDrive - Queen\'s University Belfast/University/Stage 4/Project/Data Extraction from CAD/CSV Files/Similarities/"
    modelName = input('Model Name: ')

    ent = pd.read_csv(path + f'{modelName} - Entity List.csv')
    topo = pd.read_csv(path + f'{modelName} - Topology List.csv')
    interference = pd.read_csv(path + f'{modelName} - Interference List.csv')

    A = run_rescal(path, modelName)


    print(slice_k9_creation(ent, interference))

    entity = 634
    similarities = []

    for i in range(len(A)):
        thres, k = compare_entities(A[entity], A[i], delta=1e-3)
        if thres and i != entity:
            similarities.append([entity, i+1, round(k, 5)])
            print(f"Entity: {i} with k: {k}")


    with open(csv_path + f'{modelName} - similarities.csv', "w", newline="") as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=",")
        csv_writer.writerow(['Chosen Entity', 'Similar Entity', 'k'])
        csv_writer.writerows(similarities)
