from OCC.Core.IFSelect import IFSelect_RetDone
from OCC.Core.STEPControl import STEPControl_Reader
from OCC.Core.TopAbs import TopAbs_FACE, TopAbs_SOLID
from OCC.Core._IFSelect import IFSelect_ItemsByEntity
from OCC.Extend.DataExchange import read_step_file
from OCC.Core.GCPnts import GCPnts_AbscissaPoint
from OCC.Core.BRepAdaptor import BRepAdaptor_Curve
from TopologyUtils import TopologyExplorer
from OCC.Display.SimpleGui import init_display

import time
import csv
import numpy as np
import pandas as pd
import PySimpleGUI as sg
import os
import OCCD_Basic as OCCD
import Tensor_Creation
import rescal


def write_csv(csvpath, entityList, topoList, interferenceList):
    """creates a csv file of both the entity list and topology list"""

    with open(csvpath + ' - Entity List.csv', "w", newline="") as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=",")
        csv_writer.writerow(
            ['ID', 'ShapeType', 'GeomType', 'EdgeType', 'EdgeConvexity', 'DeltaEdgeLengthRatio',
             'AreaPerimeterRatio', 'V_SA_No.Faces', 'No.BoundEntities'])
        csv_writer.writerows(entityList)

    with open(csvpath + ' - Topology List.csv', "w", newline="") as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=",")
        csv_writer.writerow(['Entity', 'BoundEntity', 'RelOr'])
        csv_writer.writerows(topoList)

    with open(csvpath + ' - Interference List.csv', "w", newline="") as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=",")
        csv_writer.writerow(['Entity', 'ContactEntity', 'InterferenceType', 'Shape_A_Faces', 'Shape_B_Faces',
                             'Shape_A_Hash_Faces', 'Shape_B_Hash_Faces', 'mindistance'])
        csv_writer.writerows(interferenceList)


class ExtractTopo(object):
    """
    This class extracts the topology information from the step file and writes it into a csv file.

    Attributes:
                name - the name of the model
                path - the location of the model
                topo - this is the topology information read from the step file using opencascade topology utilities
                entityNum - stores the entity number, [vertex number, edge number, face number, solid number]
                vID_vertex - a dictionary map between database ID and vertex object in opencascade
                eID_edge - a dictionary map between entity ID and edge object in opencascade
                fID_face - a dictionary map between entity ID and face object in opencascade
                sID_solid - a dictionary map between entity ID and solid object in opencascade
                unMatchV - unmatched vertex
                ignoreV - ignored vertex
                unMatchFAdjacent - face not matched due to it is adjacent to a disappeared face in model one
                                   or new face in model two
                unMatchFEdgeNum - face not matched due to the change of the number of bounding edges
    """

    def __init__(self):
        """Creates and initialises the variables, lists and arrays required for the object"""

        self.path = None
        self.csvpath = None
        self.c = None
        self.topo = None
        self.comparison_shape = None
        self.comparison_entity = None
        self.main_shape = None
        self.main_topo = None

        self.entityNum = [0, 0, 0, 0]
        self.vID_vertex = {}
        self.eID_edge = {}
        self.fID_face = {}
        self.sID_solid = {}

        self.vHash_ID = {}
        self.eHash_ID = {}
        self.fHash_ID = {}
        self.sHash_ID = {}

        self.face_type = {}
        self.display_individual_collisions = []

        self.unMatchV = []
        self.unMatchE = []
        self.unMatchF = []

        self.ignoreV = []
        self.ignoreE = []

        self.unMatchFAdjacent = []
        self.unMatchFEdgeNum = []

        self.entityList = []
        self.topoList = []
        self.interferenceList = []

        self.data_exchane_timer = None

        self.entList = None
        self.topList = None
        self.intList = None
        self.A = None

        self.display = None

    def data_exchange(self):
        ID = 1
        edgeToDel = []

        # Attach topology explorer to step file
        self.topo = TopologyExplorer(self.main_shape)

        # object methodology
        ID = self.get_vertices(ID, self.entityList)
        ID = self.get_edges(ID, self.entityList, edgeToDel)
        ID = self.get_faces(ID, self.entityList, self.topoList)
        self.get_solids(ID, self.entityList, self.topoList)
        self.interference_detection(self.interferenceList, self.display_individual_collisions)

        self.entList = pd.DataFrame(self.entityList,
                                    columns=['ID', 'ShapeType', 'GeomType', 'EdgeType', 'EdgeConvexity',
                                             'DeltaEdgeLengthRatio',
                                             'AreaPerimeterRatio', 'V_SA_No.Faces', 'No.BoundEntities'])
        self.topList = pd.DataFrame(self.topoList, columns=['Entity', 'BoundEntity', 'RelOr'])
        self.intList = pd.DataFrame(self.interferenceList,
                                    columns=['Entity', 'ContactEntity', 'InterferenceType', 'Shape_A_Faces',
                                             'Shape_B_Faces',
                                             'Shape_A_Hash_Faces', 'Shape_B_Hash_Faces', 'mindistance'])

        self.A = self.create_slices_run_rescal(self.entList, self.topList, self.intList)

    #    write_csv(self.csvpath, self.entityList, self.topoList, self.interferenceList)

    def data_exchange_no_interference(self):
        ID = 1
        edgeToDel = []

        # attach topology explorer to step file
        self.topo = TopologyExplorer(self.main_shape)

        # object methodology
        ID = self.get_vertices(ID, self.entityList)
        ID = self.get_edges(ID, self.entityList, edgeToDel)
        ID = self.get_faces(ID, self.entityList, self.topoList)
        self.get_solids(ID, self.entityList, self.topoList)

        self.entList = pd.DataFrame(self.entityList,
                                    columns=['ID', 'ShapeType', 'GeomType', 'EdgeType', 'EdgeConvexity',
                                             'DeltaEdgeLengthRatio',
                                             'AreaPerimeterRatio', 'V_SA_No.Faces', 'No.BoundEntities'])
        self.topList = pd.DataFrame(self.topoList, columns=['Entity', 'BoundEntity', 'RelOr'])
        self.intList = pd.DataFrame(self.interferenceList,
                                    columns=['Entity', 'ContactEntity', 'InterferenceType', 'Shape_A_Faces',
                                             'Shape_B_Faces',
                                             'Shape_A_Hash_Faces', 'Shape_B_Hash_Faces', 'mindistance'])

        self.A = self.create_slices_run_rescal(self.entList, self.topList, self.intList)

    #    write_csv(self.csvpath, self.entityList, self.topoList, self.interferenceList)

    def relationallearing(self):
        display_similarity = []

        self.compare_similar_entities(self.entList, self.topList, display_similarity)
        self.similarity_display(self.entList, display_similarity)
        self.collision_display(self.intList)

    ##########################################################
    ####                    Extraction                    ####
    ##########################################################

    def get_vertices(self, ID, entityList):
        """
        This is to get information related to vertices
        """
        self.data_exchane_timer = time.process_time()
        # list of vertices in model
        vs = list(self.topo.vertices())
        self.entityNum[0] = len(vs)

        # obtain x, y, z coordinates for each vertex, apply a dictionary ID number to each vertex
        for v in vs:
            self.vID_vertex[ID] = v
            self.vHash_ID[hash(v)] = ID
            entityList.append([ID, 0, 0, 0, 0, -1, 0, 0, 0])
            ID = ID + 1

        return ID

    def get_edges(self, ID, entityList, edgeToDel):
        """
        This is to get information related to edges. For shape.Orientation(), FORWARD==0, REVERSED==1
        The orientation concept is slightly different from NX. An edge's orientation is related to the face.
        A vertex's orientation is related to the edge and face. 0 means a vertex is a start vertex
        of an edge in this face. It will have an orientation of 1 for the same edge but another face.
        """

        # list of all edges in shape, obtain edge type and edge convexity for each edge
        es = list(self.topo.edges())
        cnt = 0
        for e in es:
            self.eHash_ID[hash(e)] = ID
            vs = list(self.topo.vertices_from_edge(e))
            if len(vs) < 2 and OCCD.edge_length(e) < 0.01:
                edgeToDel.append(ID)
            else:
                self.eID_edge[ID] = e
            edge_type = OCCD.recognise_edge_type(e)
            edge_conv = OCCD.edge_dihedral(e, self.topo)

            if edge_conv == "Convex":
                edge_convexity = 1

            elif edge_conv == "Concave":
                edge_convexity = 2

            elif edge_conv == "Smooth":
                edge_convexity = 3

            else:
                edge_convexity = 0

            vs = list(self.topo.vertices_from_edge(e))
            entityList.append([ID, 1, 0, edge_type, edge_convexity, -1, 0, 0, len(vs)])

            '''
            when exporting to step file, it is possible that some topology mistake happens. One mistake is
            an edge have only one vertex and with a small length. I tried the fixshape in opencascade, but
            doesn't seem to work. (need to ask some expert maybe). Here I just simply remove this edge from the database.
            '''

            # identifies inaccurately modelled edges in the analysis

            ID = ID + 1
            cnt = cnt + 1

        self.entityNum[1] = len(es) - len(edgeToDel)

        return ID

    def get_faces(self, ID, entityList, topoList):
        """
        This is to get information related to faces
        """
        # list of all faces in shape, obtain face type
        fs = list(self.topo.faces())
        length_list = []
        perimeter = []
        cnt = 0
        for f in fs:
            self.fHash_ID[hash(f)] = ID
            self.fID_face[ID] = f
            geom_type = OCCD.recognise_face_type(f)
            surface_area = OCCD.surface_area(f)

            # calculates the ratio of the difference in length between the longest and shortest edges of the face
            edges = list(self.topo.edges_from_face(f))
            for ee in edges:
                length = GCPnts_AbscissaPoint().Length(BRepAdaptor_Curve(ee))
                length_list.append(length)
                perimeter.append(np.sum(length_list))

            area_perimeter_ratio = surface_area / perimeter[cnt]

            max_length = max(length_list)
            min_length = min(length_list)
            edge_length_ratio = round((max_length - min_length) / ((max_length + min_length) / 2), 5)
            length_list.clear()

            entityList.append([ID, 2, geom_type, 0, 0, edge_length_ratio, area_perimeter_ratio, 0, len(edges)])

            # determine the orientation of each edge in relation to the bounding face
            ws = OCCD.ask_face_wires(f, self.topo)
            for i in range(0, len(ws)):
                es = list(self.topo.edges_from_wire(ws[i]))
                for e in es:
                    if e.Orientation() == 0:
                        topoList.append([ID, self.eHash_ID[hash(e)], 1])  # start of edge
                    elif e.Orientation() == 1:
                        topoList.append([ID, self.eHash_ID[hash(e)], -1])  # end of edge

            ID = ID + 1
            cnt = cnt + 1

        self.entityNum[2] = len(fs)

        return ID

    def get_solids(self, ID, entityList, topoList):
        """
        This is to get information related to solids
        """

        # list of all solids in shape and the faces contained in the solid
        ss = list(self.topo.solids())
        solid_IDs = []

        cnt = 0
        for s in ss:
            self.sHash_ID[hash(s)] = ID
            self.sID_solid[ID] = s
            solid_IDs.append(ID)
            fs = list(self.topo.faces_from_solids(s))
            no_faces = len(fs)
            volume = OCCD.solidvolume(s)
            surface_area = OCCD.solid_surface_area(s)

            volume_face_number_surface_area = (volume / surface_area * no_faces)
            entityList.append([ID, 3, 0, 0, 0, -1, 0, volume_face_number_surface_area, len(fs)])
            for f in fs:
                topoList.append([ID, self.fHash_ID[hash(f)], 0])

            ID = ID + 1
            cnt = cnt + 1

        self.entityNum[3] = len(ss)

        # print the quantity of each entity type identified in the shape
        print(f'\nNumber of Solids: {len(self.sHash_ID)}')
        print(f'Number of Faces: {len(self.fHash_ID)}')
        print(f'Number of Edges: {len(self.eHash_ID)}')
        print(f'Number of Vertices: {len(self.vHash_ID)}')

        total_entities = len(self.sHash_ID) + len(self.fHash_ID) + len(self.eHash_ID) + len(self.vHash_ID)

        print(f'Total Number of Entities: {total_entities}')

        print(f"\nData Exchange, Elapsed time: {time.process_time() - self.data_exchane_timer}")

        return ID

    def interference_detection(self, interferenceList, display_individual_collisions):
        """
        Determines interferences between solids
        """
        interference_time = time.process_time()
        ss = list(self.topo.solids())
        if len(ss) >= 2:
            for i in ss:
                for cnt in ss:
                    if i != cnt:
                        collision = OCCD.collision_check(ss, cnt, i, self.fHash_ID, tol=1e-3)
                        if collision[0] == 1:
                            interferenceList.append(
                                [self.sHash_ID[hash(i)], self.sHash_ID[hash(cnt)], collision[0], collision[1],
                                 collision[2],
                                 collision[3], collision[4], collision[5]])

        else:
            print("Only one solid present in model, therefore collision analysis unavailable")

        interference = pd.DataFrame(interferenceList, columns=['Entity', 'ContactEntity', 'InterferenceType',
                                                               'Shape_A_Faces', 'Shape_B_Faces', 'Shape_A_Hash_Faces',
                                                               'Shape_B_Hash_Faces', 'mindistance'])

        join = []
        shape_a_faces = interference['Shape_A_Hash_Faces']
        shape_b_faces = interference['Shape_B_Hash_Faces']
        for cnt in range(0, len(interference["Entity"])):
            join.append([*shape_a_faces[cnt], *shape_b_faces[cnt]])

        for x in join:
            display_individual_collisions.append(x)

        number_interferences = len(interference['Entity'])

        print(f"\nNumber of Interferences: {number_interferences}")
        print(f"\nNumber of intersecting faces: {sum(map(len, join))}")

        if len(interferenceList) != 0:
            print("\nCollision/Interference between solid bodies detected\n")
        else:
            print("\nNo Collision/Interference between solid bodies detected\n")

        print(f"Interference Detection, Elapsed time: {time.process_time() - interference_time}")
        print(
            f"\nData Exchange with Interference Detection, Elapsed time: {time.process_time() - self.data_exchane_timer}")

    ##########################################################
    ####                Relational Learning               ####
    ##########################################################

    def create_slices_run_rescal(self, entList, topList, intList):
        domain_creation_time = time.process_time()
        if len(self.sID_solid) == 0 or len(self.fID_face) == 0:
            print("Please Run Data Exchange First!")
            exit()
        """
        creates the frontal slices, each of which represent a different relation between entities.
        The matrix, A is created universally representing the system of relations
        between entities.
        """

        x0 = Tensor_Creation.slice_k0_creation(entList, topList)
        x1 = Tensor_Creation.slice_k1_creation(entList)
        x2 = Tensor_Creation.slice_k2_creation(entList)
        x3 = Tensor_Creation.slice_k3_creation(entList)
        x4 = Tensor_Creation.slice_k4_creation(entList)
        x5 = Tensor_Creation.slice_k5_creation(entList)
        x6 = Tensor_Creation.slice_k6_creation(entList)
        x7 = Tensor_Creation.slice_k7_creation(entList, intList)
        x8 = Tensor_Creation.slice_k8_creation(entList)
        x9 = Tensor_Creation.slice_k9_creation(entList, intList)

        self.A, R, _, _, _ = rescal.als([x0, x1, x2, x3, x5, x6, x7, x9], 3, compute_fit=True)

        print(f"\nRelational Domain Creation, Elapse time: {time.process_time() - domain_creation_time}")

        return self.A

    def compare_similar_entities(self, entList, topList, display_similarity):
        """
        The tensor is decomposed using the RESCAL method. An entity is chosen for analysis and a
        similarity value, k is determined based on the similarity of other entities to the chosen entity.
        """
        rescal_factorisation_time = time.process_time()

        similarities = []
        shape_type = entList["ShapeType"]
        entity = np.int(self.comparison_entity)
        delta_value = np.float(myTopo.delta)

        entity_shape_type = shape_type[entity - 1]
        print(f"Chosen entity: {entity}")
        if entity_shape_type < 2:
            print("\nPlease select an entity that represents a face or solid.\n")
        elif entity_shape_type >= 2:
            for i in range(len(self.A)):
                thres, k = Tensor_Creation.compare_entities(self.A[entity - 1], self.A[i], delta=delta_value)
                if thres and i != entity - 1 and shape_type[entity - 1] == shape_type[i]:
                    if k > 0.995:
                        similarities.append([entity, i + 1, k])
                        print(f"{i + 1}, {k}")

                    #    print(f"Entity: {entity} has Similarity to Entity ID: {i + 1} with k: {k}")

                    if k >= np.float(myTopo.chosen_k):
                        if shape_type[i] == 2:
                            display_similarity.append(self.fID_face[i + 1])
                        elif shape_type[i] == 3:
                            display_similarity.append(self.sID_solid[i + 1])
                        elif shape_type[i] != 2 or 3:
                            print("Please Enter an entity relating to a face or edge!")
                            exit()
        print(f"\nNumber of similarities discovered: {len(similarities)}")
        print(
            f"\nRESCAL Factorisation/Similiarity Clustering, Elapsed time: {time.process_time() - rescal_factorisation_time}")

    def similarity_display(self, entList, display_similarity):
        if len(display_similarity) > 1:
            global chosen_entity_key

            shape_type = entList["ShapeType"]

            if self.display is None:
                display, start_display, add_menu, add_function_to_menu = init_display()
                self.display = display
            # self.display.SetSelectionModeVertex()
            # self.display.EraseAll()

            self.display.DisplayShape(self.main_shape, update=True, transparency=0.9)
            chosen_entity = np.int(self.comparison_entity)

            if shape_type[chosen_entity - 1] == 2:
                chosen_entity_key = self.fID_face[chosen_entity]
            elif shape_type[chosen_entity - 1] == 3:
                chosen_entity_key = self.sID_solid[chosen_entity]
            else:
                print("The entity for comparison is not a face or solid")

            self.display.DisplayShape(chosen_entity_key, update=True, color="green")
            for i in display_similarity:
                self.display.DisplayShape(i, update=True, color="blue")

            # start_display()
        else:
            print("No directly similar entities therefore, similarity display is unavailable")

    def collision_display(self, intList):
        if len(self.display_individual_collisions) > 0:
            ss = list(self.main_topo.solids())
            if len(ss) > 1:
                sg.theme('Reddit')
                logo = [[sg.Image("QUB-logo.png")]]
                collision = [
                    [sg.Button("Display Next Interference", button_color=("black", "light green"), size=(30, 1)),
                     sg.Button("Display All Interferences", button_color=("black", "light blue"), size=(30, 1))]]

                layout1 = [
                    [sg.Column(logo, vertical_alignment='Centre', justification='Centre')],
                    [sg.Column(collision, vertical_alignment='Centre', justification='Centre')],
                    [sg.Text("Instructions: To Diplay Next Interference")],
                    [sg.Text("1. Close interference view window.")],
                    [sg.Text("2. Press Display Interference to display next interference.")],
                    [sg.Text("3. When finished, close interference view and close user interface to complete task.")]]

                window_collisions = sg.Window('Interference Menu', layout1, size=(600, 300))
                cnt = 0
                while True:
                    event1, values1 = window_collisions.read()

                    if event1 == sg.WIN_CLOSED:
                        break
                    elif event1 == "Display All Interferences":
                        display, start_display, add_menu, add_function_to_menum = init_display()
                        display.EraseAll()
                        display.DisplayShape(myTopo.main_shape, update=True, transparency=0.9)
                        for x in np.array(self.display_individual_collisions, dtype=object):
                            for y in x:
                                display.DisplayShape(y, update=True, color="blue")
                        start_display()

                    elif event1 == "Display Next Interference":
                        cnt = cnt + 1
                        display, start_display, add_menu, add_function_to_menum = init_display()
                        display.EraseAll()
                        display.DisplayShape(myTopo.main_shape, update=True, transparency=0.9)
                        face_list = np.array(self.display_individual_collisions, dtype=object)

                        for y in face_list[cnt]:
                            display.DisplayShape(y, update=True, color="blue")

                        start_display()

            else:
                print("Only one solid present in model, therefore collision display unavailable")

    def set_shape(self, shape):
        self.shape = shape
        # Need to find the matching entity for corresponding shape
        if self.shape.ShapeType() == TopAbs_FACE:
            self.comparison_entity = np.int(self.fHash_ID[hash(self.shape)])
        elif self.shape.ShapeType() == TopAbs_SOLID:
            self.comparison_entity = np.int(self.sHash_ID[hash(self.shape)])
        else:
            print("Not solid or face")

    def recognize_clicked(self, shp, *kwargs):
        """ This is the function called every time
        a face is clicked in the 3d view
        """
        for shape in shp:  # this should be a TopoDS_Face TODO check it is
            self.set_shape(shape)
            # Once you get the entity for comparison run relational learning
            relational_timer = time.process_time()
            self.relationallearing()
            print(f"Elapsed process time: {time.process_time() - relational_timer}")


if __name__ == '__main__':
    sg.theme('Reddit')
    col = [[sg.Image("QUB-logo.png")]]
    but = [[sg.Button("Load Model", button_color=("black", "orange"), size=(15, 1)),
            sg.Button("Data Exchange", button_color=("black", "light blue"), size=(15, 1)),
            sg.Button("Similarity Analysis", button_color=("black", "light green"), size=(15, 1))]]
    entries = [[sg.Text("Delta:"), sg.InputText("1e-13", key="delta", size=(15, 10)), sg.Text("Similarity:"),
                sg.InputText("0.995", key="k", size=(15, 10)),
                sg.Text("Interference Detection:"),
                sg.Button('ON', key="interference", size=(3, 1), button_color=('white', 'green'))]]

    layout = [
        [sg.Column(col, vertical_alignment='Centre', justification='Centre')],
        [sg.Text("Choose a File: ", justification="centre"), sg.Input("", key="in", size=(55, 10)),
         sg.FileBrowse(file_types=(("Step Files", "*.Step"), ("Step Files", "*.step"), ("Step Files", "*.stp")))],
        #    [sg.Text("Save Location: "), sg.Input("", key="save", size=(55, 10)), sg.FileSaveAs()],
        [sg.Column(entries, vertical_alignment='Centre', justification='Centre')],
        [sg.Column(but, vertical_alignment='Centre', justification='Centre')],
        [sg.Text("Instructions: ")],
        [sg.Text("1. To search for the desired CAD model, press 'Browse' and open the model.")],
        [sg.Text("2. Load the CAD model into the tool by pressing 'Load Model'")],
        [sg.Text("3. Interference detection can be activated/deactivated by toggling the button.")],
        [sg.Text("4. Run the data exchange process by clicking the 'Data Exchange' button")],
        [sg.Text("5. In 'Delta: ', enter the desired similarity constant/delta value")],
        [sg.Text(
            "6. In 'Similarity: ', enter the desired k value where the entites with similarity above k are returned.")],
        [sg.Text("7. Press 'Similarity Analysis' to begin the identification/display of similarity clusters.")],
        [sg.Text("8. The Output box below, contains the outputs from the tool:")],
        [sg.Output(size=(86, 40))]]

    window = sg.Window('Main Menu', layout, size=(650, 850))

    myTopo = ExtractTopo()

    down = True

    while True:
        event, values = window.read()
        myTopo.delta = values["delta"]
        myTopo.path = values["in"]
        #    myTopo.csvpath = values["save"]
        myTopo.chosen_k = values["k"]

        if event == sg.WIN_CLOSED:
            break
        elif event == "Load Model":
            modelName = os.path.basename(myTopo.path)
            myTopo.main_shape = read_step_file(myTopo.path)
            myTopo.main_topo = TopologyExplorer(myTopo.main_shape)
            print("CAD model loaded")

        elif event == "interference":
            down = not down
            window.Element("interference").Update(('OFF', 'ON')[down], button_color=(('white', ('red', 'green')[down])))

        elif event == "Data Exchange":
            if window["interference"].get_text() == 'ON':
                myTopo.data_exchange()
            else:
                myTopo.data_exchange_no_interference()


        elif event == "Similarity Analysis":
            sg.popup("In the display window:\n"
                     "Press 'G' on the keyboard 4x to activate the solid section tool.\n"
                     "\nPress 'G' on the keyboard 6x to activate the face section tool.\n"
                     "\nSelect the desired entity.", title="Similarity Display Instructions")
            # shape = read_step_file(myTopo.path)
            display, start_display, add_menu, add_function_to_menu = init_display()
            display.SetSelectionModeFace()  # switch to Face selection mode
            myTopo.display = display
            display.register_select_callback(myTopo.recognize_clicked)

            display.EraseAll()
            display.DisplayShape(myTopo.main_shape, update=True)
            start_display()
